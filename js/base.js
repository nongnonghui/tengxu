﻿function post(url,params,callback,error){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("post",BASE_URL+url);
  xmlhttp.setRequestHeader("content-Type","application/json");
  xmlhttp.send(JSON.stringify(params));
  xmlhttp.onreadystatechange = function(){
    if(xmlhttp.readyState==4&&xmlhttp.status==200){
      if(xmlhttp.responseText.length>0){
        callback(JSON.parse(xmlhttp.responseText));
      }else{
        callback({});
      }
    }else if(xmlhttp.readyState==4&&xmlhttp.status==500){
      if(error) error(xmlhttp);
    }else if(xmlhttp.readyState==4&&xmlhttp.status==0){
      alert("有错误发生，请检查您的网络！");
    }
  };
}

const BASE_URL = "http://web16.hfbdqn.cn:8080/";